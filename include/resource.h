/* Copyright (c) 2023 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#ifndef RESOURCE_H
#define RESOURCE_H

#include <stdio.h>

struct Resource {
    int protocol;
    int domain_id;
};

#endif
